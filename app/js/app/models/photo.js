'use strict';
/*global App:true DS:true */

App.Photo = DS.Model.extend({
  datetaken: DS.attr('date'),
  farm: DS.attr('number'),
  latitude: DS.attr('number'),
  longitude: DS.attr('number'),
  owner: DS.attr('string'),
  ownername: DS.attr('string'),
  secret: DS.attr('string'),
  server: DS.attr('string'),
  title: DS.attr('string')
});