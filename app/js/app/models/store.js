'use strict';
/*global App:true, DS:true, jQuery:true */

App.FlickrAdapter = DS.RESTAdapter.extend({
  findAll: function(store, type, since) {
    var root = this.rootForType(type);

    this.ajax('', "GET", {
      data: this.sinceQuery(since),
      success: function(json) {
        var photos = {};
        photos['photos'] = json.photos.photo;
        App.Search.set('isLoaded', true);
        Ember.run(this, function(){
          this.didFindAll(store, type, photos);
        });
      }
    });
  },
  ajax: function(url, type, hash) {
    hash.url = 'http://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=64d016d36f84b248845b35a736c5e321&privacy_filter=1&safe_search=1&per_page=100&format=json&extras=tags,owner_name,date_taken&text=' + App.Search.value;
    hash.type = type;
    hash.dataType = 'jsonp';
    hash.jsonpCallback = 'jsonFlickrApi';
    hash.context = this;

    if (hash.data && type !== 'GET') {
      hash.data = JSON.stringify(hash.data);
    }

    jQuery.ajax(hash);
  }
});

App.Store = DS.Store.extend({
  revision: 11,
  adapter: 'App.FlickrAdapter'
});