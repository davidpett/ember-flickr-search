'use strict';
/*global App:true Ember:true */

App.Search = Ember.Object.create({
  isLoaded: false,
  isSubmitted: false,
  value: ''
});

App.Router = Ember.Router.extend({
  location: 'none',
});

App.Router.map(function() {
  this.resource('photos', function() {
    this.resource('photo', {path:':photo_id'});
  });
});

App.ApplicationRoute = Ember.Route.extend({
  events: {
    searchTag: function() {
      App.reset();
      App.Search.set('isLoaded', false);
      App.Search.set('isSubmitted', true);
      this.replaceWith('photos');
    }
  }
});

App.PhotosRoute = Ember.Route.extend({
  model: function() {
    return App.Photo.find();
  }
});