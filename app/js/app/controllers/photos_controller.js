'use strict';
/*global App:true, Ember:true */

App.PhotosController = Ember.ArrayController.extend({
  filteredContent: function() {
    return this.content;
  }.property('content'),
  itemController: 'photo'
});