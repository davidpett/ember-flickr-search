'use strict';
/*global App:true, Ember:true */

var dayNames = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
    monthNames = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

App.PhotoController = Ember.ObjectController.extend({
  date: function() {
    var d = new Date(this.get('datetaken'));
    return dayNames[d.getDay()] + ', ' + monthNames[d.getMonth()] + ' ' + d.getDate() + ', ' + d.getFullYear();
  }.property('datetaken'),

  urlBase: function() {
    return 'http://farm' + this.get('farm') + '.staticflickr.com/' + this.get('server') + '/' + this.get('id') + '_' + this.get('secret');
  }.property('farm', 'id', 'secret', 'server'),

  urlLarge: function() {
    return this.get('urlBase') + '_c.jpg';
  }.property('urlBase'),

  urlSquare: function() {
    return this.get('urlBase') + '_q.jpg';
  }.property('urlBase')
});